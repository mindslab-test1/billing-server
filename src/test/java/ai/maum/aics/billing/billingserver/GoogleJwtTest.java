package ai.maum.aics.billing.billingserver;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.AndroidPublisherScopes;
import com.google.api.services.androidpublisher.model.SubscriptionDeferralInfo;
import com.google.api.services.androidpublisher.model.SubscriptionPurchase;
import com.google.api.services.androidpublisher.model.SubscriptionPurchasesDeferRequest;
import com.google.api.services.androidpublisher.model.SubscriptionPurchasesDeferResponse;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class GoogleJwtTest {

    String packageName = "com.dkbooktalk.dkbooktalk_app";
    String productId = "bookjam_total_1month";
    String purchaseToken = "ehhdocoiifjnakbgfoffddjo.AO-J1OwSwoz2SnUmqBflocQ-qJvLOPtT-_qKNldUqTWfzGyvZxBjgQNCH1c5GyesVmilX163g0amSYMJfhNCXiOv_mdtRAkRV5GsmgIZpQLioO1M0KNXU4wiDNpYa7bxSP2SeCxwO7x7l4xr5opduIt7FuyQwE5e4Q";

    String emailAddress = "test-568@api-7501794144505714203-23639.iam.gserviceaccount.com";
    String serviceAccountScope = "https://www.googleapis.com/auth/androidpublisher";

    @Test
    public void mainTest() throws IOException, GeneralSecurityException {

        JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

//        GoogleCredential credential = new GoogleCredential.Builder()
//                .setTransport(httpTransport)
//                .setJsonFactory(JSON_FACTORY)
//                .setServiceAccountId(emailAddress)
//                .setServiceAccountPrivateKeyFromP12File(new File("src/test/resources/security_key/junsu-service-account.p12"))
//                .setServiceAccountScopes(Collections.singleton(serviceAccountScope))
//                .build();

        GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream("src/test/resources/security_key/serviceAccount.json"))
                .createScoped(AndroidPublisherScopes.ANDROIDPUBLISHER);

        HttpCredentialsAdapter httpCredentialsAdapter = new HttpCredentialsAdapter(credentials);

        System.out.println(credentials);

        purchasesSubscriptionsGetTest(httpTransport, JSON_FACTORY, httpCredentialsAdapter);
    }

    private void purchasesSubscriptionsGetTest(HttpTransport httpTransport, JsonFactory JSON_FACTORY, HttpRequestInitializer httpRequestInitializer) throws IOException {

        AndroidPublisher publisher = new AndroidPublisher.Builder(httpTransport, JSON_FACTORY, httpRequestInitializer)
                .setApplicationName(packageName)
                .build();

        AndroidPublisher.Purchases.Subscriptions.Get get = publisher.purchases().subscriptions().get(packageName, productId, purchaseToken);
        SubscriptionPurchase productPurchase = get.execute();
        System.out.println(productPurchase.toPrettyString());
        purchasesSubscriptionsDeferTest(httpTransport, JSON_FACTORY, httpRequestInitializer, productPurchase.getExpiryTimeMillis());
    }

    private void purchasesSubscriptionsDeferTest(HttpTransport httpTransport, JsonFactory JSON_FACTORY, HttpRequestInitializer httpRequestInitializer, Long expiryTimeMillis) throws IOException {
        AndroidPublisher publisher = new AndroidPublisher.Builder(httpTransport, JSON_FACTORY, httpRequestInitializer)
                .setApplicationName(packageName)
                .build();

        SubscriptionPurchasesDeferRequest subscriptionPurchasesDeferRequest = new SubscriptionPurchasesDeferRequest();
        SubscriptionDeferralInfo subscriptionDeferralInfo = new SubscriptionDeferralInfo();

        System.out.println(expiryTimeMillis);
        System.out.println(Instant.ofEpochMilli(expiryTimeMillis).plus(5L, ChronoUnit.DAYS).toEpochMilli());

        subscriptionDeferralInfo.setExpectedExpiryTimeMillis(expiryTimeMillis);
        subscriptionDeferralInfo.setDesiredExpiryTimeMillis(Instant.ofEpochMilli(expiryTimeMillis).plus(5L, ChronoUnit.DAYS).toEpochMilli());

        subscriptionPurchasesDeferRequest.setDeferralInfo(subscriptionDeferralInfo);

        AndroidPublisher.Purchases.Subscriptions.Defer defer = publisher.purchases().subscriptions().defer(packageName, productId, purchaseToken, subscriptionPurchasesDeferRequest);
        SubscriptionPurchasesDeferResponse productPurchase = defer.execute();
        System.out.println(productPurchase.toPrettyString());
    }
}
