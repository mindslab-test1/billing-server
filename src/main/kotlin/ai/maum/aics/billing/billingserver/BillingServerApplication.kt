package ai.maum.aics.billing.billingserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BillingServerApplication

fun main(args: Array<String>) {
    runApplication<BillingServerApplication>(*args)
}
